/**
 *
 */

// Global Variables Declaration by Dhwanik Gnadhi
// $.mobile.wsurl = "http://www.theengagementworks.com/rest/users/";
var user = "";

var BASE_URL = "http://69.87.217.15:94/api/snips/";
var VIDEOLIST="currentDashboard";
var VIDEOLIBRARY="currentCourses";
var VIDEODETAILS="courseDetail";
var QUIZ="courseQuiz";
var SAVEQUIZ="Submit";
var VIEWCOURSES="viewCourse";
var EDITPROFILE="editProfile";
var NEXTQUESTION="NextQuestion";
var MYSUBSCRIPTION="currentSubscriptions";
var ADDCOMMENT="AddComment";


// Global Methods Declaration by Dhwanik Gnadhi

function promptCallback(){
    
}


function onBackKeyDown() {
    
    var activePage = $.mobile.activePage.attr("id");
    
    if (activePage == 'Login' || activePage == 'WelcomeScreen') {
        
        navigator.app.exitApp();
        
        //navigator.notification.prompt("sdfsd", promptCallback, ["title"], ["yes"], ["no"])
        
    } else {
        
        history.go(-1);
    }
}

function logoutUser() {
    
    localStorage.clear();
    changepage_history("login.html");
}

function changepage(page) {
    
    $.mobile.changePage(page, {
                        transition : "slide",
                        });
}

function changepage_history(page) {
    
    $.mobile.changePage(page, {
                        transition : "slide",
                        changeHash : true,
                        });
}

function changepage_historyNo(page) {
    
    $.mobile.changePage(page, {
                        transition : "slide",
                        changeHash : false,
                        });
}

// ValidateEmail

function ValidateEmail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        return false;
    } else {
        return true;
    }
}

// Alert Notification

function ShowAlertMessage(EnglishMessage) {
    navigator.notification.alert(
                                 EnglishMessage,  // message
                                 callBack,         // callback
                                 'PhoneGapApp Demo',            // title
                                 'Done'                  // buttonName
                                 );
}

function callBack() {
//    alert("Alert Dismiss");
}

// CheckNetworkConnection

function CheckNetworkConnection() {
    
    var networkState = navigator.connection.type;
    var states = {};
    
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';
    
    if (networkState == Connection.NONE) {
        // if no network then return false
        return false;
    } else {
        // else return true
        return true;
    }
}




//In App Browser

var inAppBrowserRef;

function OpenURL(url) {
    // alert("Browser Ref called");
    var target = "_blank";
    
    var options = "location=yes";
    
    inAppBrowserRef = cordova.InAppBrowser.open(url, target, options);
    
    
}


